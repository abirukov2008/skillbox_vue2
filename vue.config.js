const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  publicPath: '/vue-app/',
  transpileDependencies: true,
  runtimeCompiler: true,
});
